package com.lineate.traineeship;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class EntityServiceTest {

    @Test
    public void testCreateEntity() {
        UserService userService = ServiceFactory.createUserService();
        EntityService entityService = ServiceFactory.createEntityService();

        Group adminGroup = userService.createGroup("admin");
        User userBoris = userService.createUser("Boris", adminGroup);

        Entity entity = new Entity("key", "123");

        assertTrue(entityService.save(userBoris, entity));
    }

    @Test
    public void testEntitySpace() {
        EntityService entityService = ServiceFactory.createEntityService();
        Entity entity = new Entity("Entity space", "test");
        UserService userService = ServiceFactory.createUserService();
        User user = userService.createUser("User");
        assertFalse(entityService.save(user,entity));
    }
    @Test
    public void testEntityLenght() {
        EntityService entityService = ServiceFactory.createEntityService();
        Entity entity = new Entity("EntityLessThan32Characters!!!!!!!!!!!!!!!", "test");
        UserService userService = ServiceFactory.createUserService();
        User user = userService.createUser("User");
        assertFalse(entityService.save(user,entity));
    }
    @Test
    public void testEntityEmptyName() {
        EntityService entityService = ServiceFactory.createEntityService();
        UserService userService = ServiceFactory.createUserService();
        User user = userService.createUser("User");
        Entity entity = new Entity("", "test");
        assertFalse(entityService.save(user,entity));
    }

    @Test
    public void testEntityEditAndPermission() {
        UserService userService = ServiceFactory.createUserService();
        Group adminGroup = userService.createGroup("admin");
        Group groupRead = new Group("user");
        Group groupWrite = new Group("write");
        User userBoris = userService.createUser("Boris", adminGroup);
        User userIvan = userService.createUser("Ivan");
        User userSemen = userService.createUser("Semen", groupRead);
        EntityService entityService = ServiceFactory.createEntityService();
        Entity entity = new Entity("Entity", "te st");
        assertTrue(entityService.save(userBoris, entity));
        entityService.getByName(userBoris, "Entity").setValue("test2");
        assertEquals(entityService.getByName(userBoris, "Entity").getValue(), "test2");
        assertFalse(entityService.save(userSemen, entity));
        assertFalse(entityService.delete(userSemen, entity));
        entityService.grantPermission(entity, groupWrite, Permission.write);
        assertFalse(entityService.save(userSemen, entity));
        userService.addUserToGroup(userSemen, groupWrite);
        assertTrue(entityService.save(userSemen, entity));
        entity.setValue("test Semen!");
        assertTrue(entityService.delete(userSemen, entity));
        assertEquals(entity, null);

    }


    @Test (expected = NullPointerException.class)
    public void testEntityEmpty() {
        EntityService entityService = ServiceFactory.createEntityService();
        UserService userService = ServiceFactory.createUserService();
        User user = userService.createUser("ivan");
        Entity entity = null;
        assertTrue(entityService.save(user,entity));
    }

    @Test (expected = NullPointerException.class)
    public void testEntityDoesNotExist() {
        EntityService entityService = ServiceFactory.createEntityService();
        UserService userService = ServiceFactory.createUserService();
        User user = userService.createUser("ivan");
        Entity entity = entityService.getByName(user, "Entity");

    }


    @Test
    public void testEntitySetviceSetValue(){
        EntityService entityService = ServiceFactory.createEntityService();
        Entity entity = new Entity("Entity");
        UserService userService = ServiceFactory.createUserService();
        User user = userService.createUser("User");
        entityService.save(user,entity);
        entityService.getByName(user, "Entity").setValue("Value");
        assertEquals(entity.getValue(), "Value");
    }
    @Test
    public void testEntityServicePermission(){
        EntityService entityService = ServiceFactory.createEntityService();
        Entity entity1 = new Entity("Entity1","Value1");
        Entity entity2 = new Entity("Entity2","Value2");
        UserService userService = ServiceFactory.createUserService();
        Group group1 = userService.createGroup("group1");
        Group group2 = userService.createGroup("group2");
        Group group3 = userService.createGroup("group3");
        User user1 = userService.createUser("User1", group1);
        User user2 = userService.createUser("User2");
        User user3 = userService.createUser("User3",group3);
        userService.addUserToGroup(user1,group3);
        assertTrue(entityService.save(user1,entity1));
        assertTrue(entityService.save(user1,entity2));
        assertFalse(entityService.save(user2,entity1));
        assertFalse(entityService.save(user3,entity2));
        assertFalse(entityService.delete(user2,entity1));
        assertFalse(entityService.save(user2,entity2));
        assertFalse(entityService.delete(user2,entity2));
        assertFalse(entityService.delete(user3,entity2));
        entityService.grantPermission(entity2,group2,Permission.write);
        userService.addUserToGroup(user3,group2);
        assertTrue(entityService.delete(user1,entity1));
        assertTrue(entityService.delete(user3,entity2));
    }

    @Test
    public void testEntityDelete(){
        EntityService entityService = ServiceFactory.createEntityService();
        UserService userService = ServiceFactory.createUserService();
        User user1 = userService.createUser("User1");
        User user2 = userService.createUser("User2");
        Entity entity = new Entity("Entity");
        entityService.save(user1, entity);
        entityService.delete(user1,entity);
        assertFalse(entityService.save(user2,entity));

    }
}
