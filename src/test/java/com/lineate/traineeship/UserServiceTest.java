package com.lineate.traineeship;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserServiceTest {

    @Test
    public void testCreateUserWithDefaultGroup() {
        UserService userService = ServiceFactory.createUserService();

        User boris = userService.createUser("Boris");

        SoftAssertions.assertSoftly(softy -> {
            softy.assertThat(boris).isNotNull()
                    .extracting(User::getName).isEqualTo("Boris");
            softy.assertThat(boris.getGroups())
                    .containsOnly(new Group("Boris"));
        });
    }

    @Test
    public void testCreateUserWithGroup() {
        UserService userService = ServiceFactory.createUserService();
        Group group = new Group("user");
        User ivan = userService.createUser("Ivan", group);
        group.addUser(ivan);
        SoftAssertions.assertSoftly(softy -> {
            softy.assertThat(ivan.getGroups()).containsOnly(new Group("user"));
        });
    }

    @Test
    public void testCreateUserWithoutName() {
        UserService userService = ServiceFactory.createUserService();
        User ivan = userService.createUser("");
        SoftAssertions.assertSoftly(softy -> {
            softy.assertThat(ivan.getGroups()).containsOnly(new Group("")).isEmpty();
            softy.assertThat(ivan.getName()).isEmpty();
        });
    }

    @Test
    public void testCreateUserEmptyName()  {
        UserService userService = ServiceFactory.createUserService();
        User emptyName = userService.createUser("");
        assertFalse(emptyName.getName().isEmpty());
    }

    @Test(expected = NullPointerException.class)
    public void testCreateUserNullName()  {
        UserService userService = ServiceFactory.createUserService();
        userService.createUser(null);
    }

    @Test
    public void testCreateUserWithTwoGroups()  {
        UserService userService = ServiceFactory.createUserService();
        Group groupUser = userService.createGroup("Group user");
        Group groupAdmin = userService.createGroup("Group admin");

        User user = userService.createUser("Ivan", groupUser);
        user.getGroups().add(groupAdmin);
        assertFalse(user.getGroups().contains(new Group("Ivan")));
        assertTrue(user.getGroups().contains(new Group("Group admin")));
        assertTrue(user.getGroups().contains(new Group("Group user")));
    }

    @Test (expected = NullPointerException.class)
    public void testEmtpyGroup() throws NullPointerException {
        UserService userService = ServiceFactory.createUserService();
            Group empty = null;
            userService.createUser("ivan", empty);
    }



}
